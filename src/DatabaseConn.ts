import {createPool} from "oracledb";
const CONFIG1 = require('../index.json');


let user: string;
let password: string;
let connectString:string;

export class Database {
     
    createPoolDatabase() {
        return new Promise<void>((resolve:any, reject:any) => {
                switch (CONFIG1.env) {
                    case 'DEV':
                        user = CONFIG1.oracle_database_connection_dev?.username
                        password = CONFIG1.oracle_database_connection_dev?.password
                        connectString = CONFIG1.oracle_database_connection_dev?.connectString
                        break;
                    case 'QA':
                        user = CONFIG1.oracle_database_connection_qa?.username
                        password = CONFIG1.oracle_database_connection_qa?.password
                        connectString = CONFIG1.oracle_database_connection_qa?.connectString
                        break;
                    case 'PROD':
                        user = CONFIG1.oracle_database_connection?.username
                        password = CONFIG1.oracle_database_connection?.password
                        connectString = CONFIG1.oracle_database_connection?.connectString
                        break;
                    default:
                        user = CONFIG1.oracle_database_connection_dev?.username
                        password = CONFIG1.oracle_database_connection_dev?.password
                        connectString = CONFIG1.oracle_database_connection_dev?.connectString
                        break;
                } 

                
                createPool({
                    user: user,
                    password: password,
                    connectString: connectString
                   
                }, (err, pool) => {
                    if (err) reject(err);
            
                    resolve(` -- Conexion ORACLE Exitosa  -- ${CONFIG1.env}`);
                })
 
        })
    }

    
}
