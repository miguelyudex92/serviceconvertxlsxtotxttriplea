const AWS = require('aws-sdk');
const configuration = require('../../index.json');

const REGION = "us-east-1"; //Virginia del norte

AWS.config.update({

    accessKeyId: configuration.AWS.accessKeyId,
    secretAccessKey : configuration.AWS.secretAccessKey,
    region: REGION,
    correctClockSkew : true

});

const s3Client = new AWS.S3();
export { s3Client };