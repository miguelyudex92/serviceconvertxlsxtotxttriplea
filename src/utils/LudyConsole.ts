
const configs = require('../../index.json')
import moment from "moment";

export default class LudyConsole {
    private  appname:string = configs.app_name;

    public info(message: string, type: string = "INFO") {
        let dateTime = moment(new Date()).format("DD/MM/YYYY HH:mm:ss");
        console.log(`[${ dateTime }] [${ type }] ${ this.appname }: ${ message }`);
    }

    public start(message: string) {
        console.info(message, "START");
    }

    public end(message: string) {
        console.info(message, "END");
    }

    public error(message: string) {
        let dateTime = moment(new Date()).format("DD/MM/YYYY HH:mm:ss");
        console.error(`[${ dateTime }] [ERROR] ${ this.appname }: ${ message }`);
        //this.info(message, "ERROR");
    }
}

