export class OracleException{
  
    message: string;
    stack: Error;
      
    constructor(message:string){
      this.message=message; 
      this.stack=new Error(this.message);
    }

}