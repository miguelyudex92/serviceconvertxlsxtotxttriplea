import fs from "fs";
const path = require('path');
const xlsxFile = require('read-excel-file/node');

export default class Utilities {

    public async readXlsxFile() {
        return new Promise<[]>(async (resolve, reject) => {
            xlsxFile(path.join(__dirname, '/ZONAZ08-BQ.xlsx'))
                .then((rows: any) => {
                    resolve(rows)
                })
                .catch((err: any) => {
                    console.log(err);
                    reject(err);
                });
        })
    }
}
