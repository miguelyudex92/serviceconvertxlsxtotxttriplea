import { RequestXlsxOsf } from '../controllers/index.controller';
import { DataLudyImpoDTO } from '../DTO/DataLudyImpo.dto';
import fs from 'fs';

const path = require('path');
const globalConfig = require('../../index.json');


const dataLudyImpoDto: DataLudyImpoDTO = new DataLudyImpoDTO();

export class GenerateTxtFile {

    public homologationData(dataRequestFile: []) {
        let arrayData: object[] = []
        return new Promise<any>(async (resolve: any, reject: any) => {

            dataRequestFile.forEach(async (element: any) => {
                let objectData: object = {}
                if (element[4] === null) {
                    reject("El campo ciclo/Zona no puede ser nullo")
                }
                let ciclo: string = element[4].substring(0, 3)

                dataLudyImpoDto.estructuraArchivoPlano = element[3]
                dataLudyImpoDto.cicloZonaInmueble = ciclo
                dataLudyImpoDto.loteDeLectura = this.concatenaString("", 2, "0")
                dataLudyImpoDto.anio = element[5]
                dataLudyImpoDto.mes = String(element[6]).length < 2 ? `0${element[6]}` : String(element[6])
                dataLudyImpoDto.nombreDelCliente = 1234567
                dataLudyImpoDto.direccionDelPredio = this.concatenaString(element[8], 30, " ")
                dataLudyImpoDto.rutaRecorrido = this.concatenaString(String(element[9]), 14, "0")
                dataLudyImpoDto.emplazamiento = 11
                dataLudyImpoDto.medidor = this.concatenaString(element[10], 30, " ")
                dataLudyImpoDto.observacionDeLectura = this.concatenaString(element[11], 36, " ")
                dataLudyImpoDto.nombreDelCliente2 = this.concatenaString(element[12], 40, " ")
                dataLudyImpoDto.lecturaPrimerPeriodo = this.concatenaString("", 8)
                dataLudyImpoDto.consumoPromedioPrimerPeriodo = this.concatenaString("", 6)
                dataLudyImpoDto.observacionPrimerPeriodo = this.concatenaString("", 2)
                dataLudyImpoDto.origenLecturaPrimerPeriodo = this.concatenaString("", 1)
                dataLudyImpoDto.lecturaSegundoPeriodo = this.concatenaString("", 8)
                dataLudyImpoDto.consumoPromedioSegundoPeriodo = this.concatenaString("", 6)
                dataLudyImpoDto.observacionSegundoPeriodo = this.concatenaString("", 2)
                dataLudyImpoDto.origenLecturaSegundoPeriodo = this.concatenaString("", 1)
                dataLudyImpoDto.lecturaTercerPeriodo = this.concatenaString("", 8)
                dataLudyImpoDto.consumoPromedioTercerPeriodo = this.concatenaString("", 6)
                dataLudyImpoDto.observacionTercerPeriodo = this.concatenaString("", 2)
                dataLudyImpoDto.origenLecturaTercerPeriodo = this.concatenaString("", 1)
                dataLudyImpoDto.lecturaCuartoPeriodo = this.concatenaString("", 8)
                dataLudyImpoDto.consumoPromedioCuartoPeriodo = this.concatenaString("", 6)
                dataLudyImpoDto.observacionCuartoPeriodo = this.concatenaString("", 2)
                dataLudyImpoDto.origenLecturaCuartoPeriodo = this.concatenaString("", 1)
                dataLudyImpoDto.lecturaPeriodoAnterior = element[13]
                dataLudyImpoDto.lecturaEsperadaInferior = element[14]
                dataLudyImpoDto.lecturaEsperadaSuperior = element[15]
                dataLudyImpoDto.marcaMedidor = element[10].split("-")[0]
                /* dataLudyImpoDto.empresa = globalConfig?.company
                dataLudyImpoDto.tipoDeTrabajo = globalConfig?.jobtype[0] */

                Object.assign(objectData, JSON.parse(JSON.stringify(dataLudyImpoDto)));
                arrayData.push(objectData);
            });
            console.log(`Total de filas en archivo : ${arrayData.length}`)
            resolve(arrayData);
        })
    }

    /**
     * Metodo que genera archivo txt con la estructura que espera la operacion LudyImpo
     * @param {object[]} data Array de objetos con las ordenes a cargar de lecturas
     */
    public generateTxtFile(data: object[]) {
        return new Promise(async (resolve, reject) => {
            let jumpLine: string = "\r\n";
            let nameFile: string = "output";
            let route: string = path.join(__dirname, `${nameFile}.txt`);
            let stream: fs.WriteStream = fs.createWriteStream(route, { flags: 'w' });
            data.forEach((item:any, index) => {
                stream.write(
                    `${item.estructuraArchivoPlano}${item.cicloZonaInmueble}${item.loteDeLectura}${item.anio}${item.mes}${item.nombreDelCliente}${item.direccionDelPredio}${item.rutaRecorrido}${item.emplazamiento}${item.medidor}${item.observacionDeLectura}${item.lecturaPrimerPeriodo}${item.consumoPromedioPrimerPeriodo}${item.observacionPrimerPeriodo}${item.origenLecturaPrimerPeriodo}${item.lecturaSegundoPeriodo}${item.consumoPromedioSegundoPeriodo}${item.observacionSegundoPeriodo}${item.origenLecturaSegundoPeriodo}${item.lecturaTercerPeriodo}${item.consumoPromedioTercerPeriodo}${item.observacionTercerPeriodo}${item.origenLecturaTercerPeriodo}${item.lecturaCuartoPeriodo}${item.consumoPromedioCuartoPeriodo}${item.observacionCuartoPeriodo}${item.origenLecturaCuartoPeriodo}${item.lecturaPeriodoAnterior}${item.lecturaEsperadaInferior}${item.lecturaEsperadaSuperior}${item.marcaMedidor}${jumpLine}`
                    )
            })
            resolve(`file ${nameFile}.txt created in folder successfully`);
        })
    }


    /**
     * Metodo que genera un string concatenado y si el parametro long es igual al length del string devuelve el mismo string
     * @param {string} dataString string base
     * @param {number} long Longitud que debe tener el string
     * @param {string} concatenate String que se desea concatenar para completar la longitud solicitada, por defecto es "1"
     * 
     */
    private concatenaString(dataString: string, long: number , concatenate:string = "1") {

        if (dataString.length === long) {
            return dataString
        } else {
            let reduce =  long - dataString.length 
            
            for (let index = 0; index < reduce; index++) {
                dataString += concatenate ?? "1" 
            }
            return dataString
        }
    }

}