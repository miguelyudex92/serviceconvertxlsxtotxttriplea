
export class DataLudyImpoDTO {

    estructuraArchivoPlano? : string
    cicloZonaInmueble? : string
    loteDeLectura? : string
    anio? : number
    mes? : string
    nombreDelCliente? : number
    direccionDelPredio? : string
    rutaRecorrido? : string
    emplazamiento? : number
    medidor? : string
    observacionDeLectura? : string
    nombreDelCliente2? : string
    lecturaPrimerPeriodo? : string
    consumoPromedioPrimerPeriodo? : string
    observacionPrimerPeriodo? : string
    origenLecturaPrimerPeriodo? : string
    lecturaSegundoPeriodo? : string
    consumoPromedioSegundoPeriodo? : string
    observacionSegundoPeriodo? : string
    origenLecturaSegundoPeriodo? : string
    lecturaTercerPeriodo? : string
    consumoPromedioTercerPeriodo? : string
    observacionTercerPeriodo? : string
    origenLecturaTercerPeriodo? : string
    lecturaCuartoPeriodo? : string
    consumoPromedioCuartoPeriodo? : string
    observacionCuartoPeriodo? : string
    origenLecturaCuartoPeriodo? : string
    lecturaPeriodoAnterior? : string
    lecturaEsperadaInferior?: number
    lecturaEsperadaSuperior? : number
    marcaMedidor? : string
    empresa? : number
    tipoDeTrabajo?: number
    
}