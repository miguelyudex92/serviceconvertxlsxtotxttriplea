import express, {Router} from "express";
import ControllerService from "../controllers/index.controller";


const controllerService : ControllerService = new ControllerService();


const router: Router = express.Router();

router.post("/managementImports", controllerService.importFile);

module.exports = router;