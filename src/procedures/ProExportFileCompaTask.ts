import { OperatorEnum } from "../Enum/OperatorEnum"
import LudyConsole from "../utils/LudyConsole";
import { OracleException } from "../exceptions/OracleException";
import OracleDB, { Lob } from "oracledb";


OracleDB.outFormat = OracleDB.OUT_FORMAT_OBJECT
OracleDB.fetchAsString = [OracleDB.DB_TYPE_RAW, OracleDB.DATE, OracleDB.NCLOB]
/* OracleDB.prefetchRows = 200 */


let ludyConsole: LudyConsole = new LudyConsole()
let dataFetch: any;

export class ProExportFileCompaTask {
    /**
     * consulta el procedimiento
     */
    run(procedureName: string, companyId: any, trabTip: number, state: number, initDate: string, finishDate: string, crudEnum: OperatorEnum): Promise<any> {

        return new Promise(async (resolve: any, reject: any) => {
            try {

                if (!procedureName) throw new Error("El nombre del procedimiento es requerido");
                if (!companyId) throw new Error("El id de la compañía es requerido");
                if (!trabTip) throw new Error("El tipo de trabajo es requerido");
                if (!initDate) throw new Error("La fecha inicial es requerida");
                if (!finishDate) throw new Error("La fecha final es requerida");
                if (!crudEnum) throw new Error("El tipo de función es requerido");

                OracleDB.getPool().getConnection()
                    .then(async (connection: OracleDB.Connection) => {

                        connection.execute(
                            `BEGIN ${procedureName}(
                            :inucompanyid,
                            :inutrabtip,
                            :inustate,
                            :initdate, 
                            :finishdate,
                            :oclorders,
                            :onuerrorcode,
                            :osberrormessage
                            
                        ); 
                        END;`,
                            {
                                inucompanyid: {
                                    type: OracleDB.NUMBER,
                                    val: companyId
                                },

                                inutrabtip: {
                                    type: OracleDB.NUMBER,
                                    val: trabTip
                                },
                                inustate: {
                                    type: OracleDB.NUMBER,
                                    val: state
                                },
                                initdate: {
                                    type: OracleDB.STRING,
                                    val: initDate
                                },
                                finishdate: {
                                    type: OracleDB.STRING,
                                    val: finishDate
                                },
                                oclorders: {
                                    type: OracleDB.CLOB,
                                    dir: OracleDB.BIND_OUT
                                },
                                onuerrorcode: {
                                    type: OracleDB.NUMBER,
                                    dir: OracleDB.BIND_OUT,
                                },
                                osberrormessage: {
                                    type: OracleDB.STRING,
                                    dir: OracleDB.BIND_OUT
                                }

                            },

                            async (err: any, result: any) => {
                                try {

                                    if (err) throw new OracleException(err.message);

                                    const output: any = result.outBinds;
                                    if (output.osbErrorMessage != null) throw new Error(output.osbErrorMessage);

                                    let ordersRaw: Lob = output?.oclorders
                                    try {
                                        dataFetch = await ordersRaw.getData();
                                    } catch (error) {
                                        reject(error);
                                    }

                                    switch (crudEnum) {
                                        case OperatorEnum.COMMIT:
                                            connection.commit((error: any) => {
                                                if (error) throw new Error(error)
                                                connection.close(async (e: any) => {
                                                    if (e) ludyConsole.error(`Ocurrió un error al cerrar la conexión: ${JSON.stringify(e)}`);
                                                    resolve(dataFetch);
                                                })
                                            })
                                            break;
                                        case OperatorEnum.ROLLBACK:
                                            connection.rollback((error: any) => {
                                                if (error) throw new Error(error)
                                                connection.close(async (e: any) => {
                                                    if (e) ludyConsole.error(`Ocurrió un error al cerrar la conexión: ${JSON.stringify(e)}`);
                                                    resolve(dataFetch);

                                                });
                                            });
                                            break
                                        case OperatorEnum.READ:
                                            connection.close(async (e: any) => {
                                                if (e) ludyConsole.error(`Ocurrió un error al cerrar la conexión: ${JSON.stringify(e)}`);
                                                resolve(dataFetch);
                                            });
                                            break
                                        default:
                                            connection.close(async (e: any) => {
                                                if (e) ludyConsole.error(`Ocurrió un error al cerrar la conexión: ${JSON.stringify(e)}`);

                                                resolve(dataFetch);
                                            });
                                            break;
                                    }


                                } catch (except: any) {

                                    connection.rollback((error: any) => {
                                        if (error) throw new Error(error)
                                        connection.close(async (e: any) => {
                                            if (e) ludyConsole.error(`Ocurrió un error al cerrar la conexión: ${JSON.stringify(e)}`);

                                            reject(except.message);

                                        });
                                    });
                                }
                            }

                        )
                    }, (error: any) => {
                        reject(error)
                    });

            } catch (error) {
                ludyConsole.error(JSON.stringify(error));
                reject(error);
            }
        })
    }
}