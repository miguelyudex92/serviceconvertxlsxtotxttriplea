import { Request, Response } from "express";
import Utilities from "../business/Utilities";
import {GenerateTxtFile} from "../business/GenerateTxtFile"

const generateTxtFile : GenerateTxtFile = new GenerateTxtFile();
const util: Utilities = new Utilities();

export interface RequestXlsxOsf {
    id: string,
    actividad: number,
    orden: number,
    noEspecificado: string,
    cicloFacturacion: string,
    anio: string,
    mes: number,
    cuenta : number,
    direccion : string,
    recorrido : number,
    numElemento : string,
    ubicacion : string,
    nombres : string,
    lecturaPrevia : number,
    limiteInferior: number,
    limiteSuperior : number,
    facturacion: number,
    periodoActual : string
}

let typesRequestXlsxOsf: RequestXlsxOsf;

export default class ControllerService {

    public async importFile(req: Request, res: Response, next: any) {
        
        /* typesRequest = req.body */
        /* console.log(req.body); */
        res.status(200).json({ message: "Solicitud recibida, se procederá a procesar archivo." });
        try {
            let dataRaw:[] =  await util.readXlsxFile();
            let headerXlsx:any = dataRaw.shift();
            try {
                let homologationData:any = await generateTxtFile.homologationData(dataRaw);
                console.log(homologationData);
                await generateTxtFile.generateTxtFile(homologationData)
            } catch (error) {
                console.log(error);
            }
            
        } catch (error) {
            console.error(error)
        }
    }


}