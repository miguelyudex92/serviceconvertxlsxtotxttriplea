import {ProExportFileCompaTask} from '../procedures/ProExportFileCompaTask';
import {OperatorEnum} from '../Enum/OperatorEnum'

let proExportFileCompaTask : ProExportFileCompaTask =  new ProExportFileCompaTask();
let procedure: string = "LUDYORDER_PROEXPORTFILECOMPATASK"


export class GetJsonDataOrders {

    public async getDataOrders(companyId:number, trabTip:number, state:number, initDate:string, finishDate:string){
        return new Promise(async(resolve, reject) => {
            try {
                let jsonData: object =  proExportFileCompaTask.run(procedure, companyId, trabTip, state, initDate, finishDate, OperatorEnum.READ);
                resolve(jsonData);
            } catch (error) {
                reject(error);
            }
        })

    }

}