import express, {Request, response, Response} from "express";
import bodyParser from "body-parser";
import {Database} from './DatabaseConn'
import LudyConsole from './utils/LudyConsole'

const cors = require('cors');
const https = require('https');
const fs = require('fs');
const serviceRoutes = require("./routes/index.routes");
require('dotenv').config();

const databaseOracle : Database = new Database();
const ludyConsole : LudyConsole = new LudyConsole();
const app = express();
const port = process.env.PORT || 4040

app.use(bodyParser.json());
app.use(cors());

let server = app.listen(port, async() => {
    console.log(`Service Up on port ${port}.`);
    //let response:any = await databaseOracle.createPoolDatabase();
    //ludyConsole.start(response);
});

app.get('/', (req: Request, res: Response) => {
    res.status(200).send(`Management Import Triple A - Service Up
        ${new Date().toLocaleString()} (${port})`);
});
app.use("/service/v1", serviceRoutes);
