"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.s3Client = void 0;
var AWS = require('aws-sdk');
var configuration = require('../../index.json');
var REGION = "us-east-1"; //Virginia del norte
AWS.config.update({
    accessKeyId: configuration.AWS.accessKeyId,
    secretAccessKey: configuration.AWS.secretAccessKey,
    region: REGION,
    correctClockSkew: true
});
var s3Client = new AWS.S3();
exports.s3Client = s3Client;
