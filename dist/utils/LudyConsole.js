"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var configs = require('../../index.json');
var moment_1 = __importDefault(require("moment"));
var LudyConsole = /** @class */ (function () {
    function LudyConsole() {
        this.appname = configs.app_name;
    }
    LudyConsole.prototype.info = function (message, type) {
        if (type === void 0) { type = "INFO"; }
        var dateTime = moment_1.default(new Date()).format("DD/MM/YYYY HH:mm:ss");
        console.log("[" + dateTime + "] [" + type + "] " + this.appname + ": " + message);
    };
    LudyConsole.prototype.start = function (message) {
        console.info(message, "START");
    };
    LudyConsole.prototype.end = function (message) {
        console.info(message, "END");
    };
    LudyConsole.prototype.error = function (message) {
        var dateTime = moment_1.default(new Date()).format("DD/MM/YYYY HH:mm:ss");
        console.error("[" + dateTime + "] [ERROR] " + this.appname + ": " + message);
        //this.info(message, "ERROR");
    };
    return LudyConsole;
}());
exports.default = LudyConsole;
