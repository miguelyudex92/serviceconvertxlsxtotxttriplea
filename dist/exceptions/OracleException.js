"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.OracleException = void 0;
var OracleException = /** @class */ (function () {
    function OracleException(message) {
        this.message = message;
        this.stack = new Error(this.message);
    }
    return OracleException;
}());
exports.OracleException = OracleException;
