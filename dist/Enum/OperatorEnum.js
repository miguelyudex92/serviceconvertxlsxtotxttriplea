"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.OperatorEnum = void 0;
var OperatorEnum;
(function (OperatorEnum) {
    OperatorEnum[OperatorEnum["COMMIT"] = 1] = "COMMIT";
    OperatorEnum[OperatorEnum["ROLLBACK"] = 2] = "ROLLBACK";
    OperatorEnum[OperatorEnum["READ"] = 3] = "READ";
})(OperatorEnum = exports.OperatorEnum || (exports.OperatorEnum = {}));
