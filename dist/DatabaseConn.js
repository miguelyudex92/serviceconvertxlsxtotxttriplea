"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Database = void 0;
var oracledb_1 = require("oracledb");
var CONFIG1 = require('../index.json');
var user;
var password;
var connectString;
var Database = /** @class */ (function () {
    function Database() {
    }
    Database.prototype.createPoolDatabase = function () {
        return new Promise(function (resolve, reject) {
            var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l, _m;
            switch (CONFIG1.env) {
                case 'DEV':
                    user = (_a = CONFIG1.oracle_database_connection_dev) === null || _a === void 0 ? void 0 : _a.username;
                    password = (_b = CONFIG1.oracle_database_connection_dev) === null || _b === void 0 ? void 0 : _b.password;
                    connectString = (_c = CONFIG1.oracle_database_connection_dev) === null || _c === void 0 ? void 0 : _c.connectString;
                    break;
                case 'QA':
                    user = (_d = CONFIG1.oracle_database_connection_qa) === null || _d === void 0 ? void 0 : _d.username;
                    password = (_e = CONFIG1.oracle_database_connection_qa) === null || _e === void 0 ? void 0 : _e.password;
                    connectString = (_f = CONFIG1.oracle_database_connection_qa) === null || _f === void 0 ? void 0 : _f.connectString;
                    break;
                case 'PROD':
                    user = (_g = CONFIG1.oracle_database_connection) === null || _g === void 0 ? void 0 : _g.username;
                    password = (_h = CONFIG1.oracle_database_connection) === null || _h === void 0 ? void 0 : _h.password;
                    connectString = (_j = CONFIG1.oracle_database_connection) === null || _j === void 0 ? void 0 : _j.connectString;
                    break;
                default:
                    user = (_k = CONFIG1.oracle_database_connection_dev) === null || _k === void 0 ? void 0 : _k.username;
                    password = (_l = CONFIG1.oracle_database_connection_dev) === null || _l === void 0 ? void 0 : _l.password;
                    connectString = (_m = CONFIG1.oracle_database_connection_dev) === null || _m === void 0 ? void 0 : _m.connectString;
                    break;
            }
            oracledb_1.createPool({
                user: user,
                password: password,
                connectString: connectString
            }, function (err, pool) {
                if (err)
                    reject(err);
                resolve(" -- Conexion ORACLE Exitosa  -- " + CONFIG1.env);
            });
        });
    };
    return Database;
}());
exports.Database = Database;
