"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var _a, _b, _c, _d;
Object.defineProperty(exports, "__esModule", { value: true });
var fs_1 = __importDefault(require("fs"));
var moment_1 = __importDefault(require("moment"));
var LudyConsole_1 = __importDefault(require("../utils/LudyConsole"));
var S3Client_1 = require("../utils/S3Client");
var uuid_1 = require("uuid");
var confs = require('../../index.json');
var path = require('path');
var ludyConsole = new LudyConsole_1.default();
var configs;
var isBucketName;
switch (confs === null || confs === void 0 ? void 0 : confs.env) {
    case "DEV":
        isBucketName = (_a = confs === null || confs === void 0 ? void 0 : confs.AWS) === null || _a === void 0 ? void 0 : _a.bucketCloud01Dev;
        break;
    case "QA":
        isBucketName = (_b = confs === null || confs === void 0 ? void 0 : confs.AWS) === null || _b === void 0 ? void 0 : _b.bucketCloud01QA;
        break;
    case "PROD":
        isBucketName = (_c = confs === null || confs === void 0 ? void 0 : confs.AWS) === null || _c === void 0 ? void 0 : _c.bucketCloud01Prod;
        break;
    default:
        isBucketName = (_d = confs === null || confs === void 0 ? void 0 : confs.AWS) === null || _d === void 0 ? void 0 : _d.bucketCloud01Dev;
        break;
}
var FileManagement = /** @class */ (function () {
    function FileManagement() {
        this.bucket = isBucketName;
    }
    /**
     * Manage json data
     */
    FileManagement.prototype.manageJsonData = function (data, pathRoute) {
        return __awaiter(this, void 0, void 0, function () {
            var dataJson, company, trabTipo, dateOrder, orderId, orders, arrayFiles, key, argsFileJson;
            var _this = this;
            return __generator(this, function (_a) {
                dataJson = data.Data;
                company = data.compania;
                trabTipo = data.tipotrabajo;
                argsFileJson = { company: company, trabTipo: trabTipo, pathRoute: pathRoute };
                dataJson.forEach(function (Data) { return __awaiter(_this, void 0, void 0, function () {
                    var error_1, error_2;
                    var _this = this;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                dateOrder = Data.FECHA;
                                orders = Data.ordenes;
                                if (orders.length === 0)
                                    return [2 /*return*/, ludyConsole.info("No existen ordenes para fecha solicitada " + dateOrder + ".")];
                                orders.forEach(function (order, index, array) { return __awaiter(_this, void 0, void 0, function () {
                                    var routeFolder;
                                    var _this = this;
                                    return __generator(this, function (_a) {
                                        switch (_a.label) {
                                            case 0:
                                                orderId = order === null || order === void 0 ? void 0 : order.Orderid;
                                                ludyConsole.info("OrderID: " + orderId + " - Fecha: " + dateOrder, 'manageJsonData');
                                                configs = { company: company, trabTipo: trabTipo, dateOrder: dateOrder, orderId: orderId, pathRoute: pathRoute };
                                                return [4 /*yield*/, this.createDirs(configs)];
                                            case 1:
                                                routeFolder = _a.sent();
                                                arrayFiles = order === null || order === void 0 ? void 0 : order.File;
                                                //Se itera array de files que pertenecen a la orden
                                                arrayFiles.forEach(function (file) { return __awaiter(_this, void 0, void 0, function () {
                                                    var error_3;
                                                    return __generator(this, function (_a) {
                                                        switch (_a.label) {
                                                            case 0:
                                                                key = file === null || file === void 0 ? void 0 : file.STORAGEID;
                                                                _a.label = 1;
                                                            case 1:
                                                                _a.trys.push([1, 3, , 4]);
                                                                return [4 /*yield*/, this.getObjectS3(this.bucket, key, routeFolder)];
                                                            case 2:
                                                                _a.sent();
                                                                return [3 /*break*/, 4];
                                                            case 3:
                                                                error_3 = _a.sent();
                                                                console.log(error_3);
                                                                return [3 /*break*/, 4];
                                                            case 4: return [2 /*return*/];
                                                        }
                                                    });
                                                }); });
                                                return [2 /*return*/];
                                        }
                                    });
                                }); });
                                _a.label = 1;
                            case 1:
                                _a.trys.push([1, 3, , 4]);
                                return [4 /*yield*/, this.saveTxtOrdersByDate(configs, orders)];
                            case 2:
                                _a.sent();
                                ludyConsole.end('== Descarga de archivos Finalizada ==');
                                return [3 /*break*/, 4];
                            case 3:
                                error_1 = _a.sent();
                                ludyConsole.error("Ocurri\u00F3 un error al generar archivo .txt " + JSON.stringify(error_1));
                                return [3 /*break*/, 4];
                            case 4:
                                _a.trys.push([4, 6, , 7]);
                                return [4 /*yield*/, this.saveJsonFile(argsFileJson, data)];
                            case 5:
                                _a.sent();
                                return [3 /*break*/, 7];
                            case 6:
                                error_2 = _a.sent();
                                ludyConsole.error(JSON.stringify(error_2));
                                return [3 /*break*/, 7];
                            case 7: return [2 /*return*/];
                        }
                    });
                }); });
                return [2 /*return*/];
            });
        });
    };
    /**
     * Create folders
     * @param {object} config
     */
    FileManagement.prototype.createDirs = function (config) {
        return new Promise(function (resolve, reject) {
            ludyConsole.start('Iniciando creación de directorios...');
            try {
                var route = path.join("" + config.pathRoute, String(config.company), String(config.trabTipo), config.dateOrder, config.orderId).toString();
                fs_1.default.mkdirSync(route, { recursive: true });
                ludyConsole.end('Directorios creados satisfactoriamente.');
                resolve(route);
            }
            catch (error) {
                console.error(error);
                reject(error);
            }
        });
    };
    /**
     * Get objects from s3
     */
    FileManagement.prototype.getObjectS3 = function (bucket, key, pathFile, folder) {
        var _this = this;
        if (folder === void 0) { folder = ''; }
        return new Promise(function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
            var slash, params, fileObject, extFile, fileRaw, error_4, error_5;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        slash = '/';
                        params = {
                            Bucket: bucket,
                            Key: "" + (folder.length > 0 ? folder + slash : '') + key
                        };
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 7, , 8]);
                        return [4 /*yield*/, S3Client_1.s3Client.getObject(params).promise()];
                    case 2:
                        fileObject = _a.sent();
                        extFile = fileObject === null || fileObject === void 0 ? void 0 : fileObject.ContentType.split('/')[1];
                        fileRaw = fileObject === null || fileObject === void 0 ? void 0 : fileObject.Body;
                        _a.label = 3;
                    case 3:
                        _a.trys.push([3, 5, , 6]);
                        return [4 /*yield*/, this.saveObjectsLocalFolder(pathFile, key + "." + extFile, fileRaw)];
                    case 4:
                        _a.sent();
                        resolve();
                        return [3 /*break*/, 6];
                    case 5:
                        error_4 = _a.sent();
                        reject("Ha ocurrido un error al intentar guardar el objeto " + key + " en la ruta " + pathFile);
                        return [3 /*break*/, 6];
                    case 6: return [3 /*break*/, 8];
                    case 7:
                        error_5 = _a.sent();
                        ludyConsole.error(JSON.stringify(error_5));
                        reject("Ha ocurrido un error al obtener el objeto " + key + " en bucket " + bucket);
                        return [3 /*break*/, 8];
                    case 8: return [2 /*return*/];
                }
            });
        }); });
    };
    /**
     * Save object in local folder
     */
    FileManagement.prototype.saveObjectsLocalFolder = function (pathFile, fileName, file) {
        return new Promise(function (resolve, reject) {
            try {
                ludyConsole.start("Iniciando descarga de archivo " + fileName);
                console.time('time-downloading-file');
                fs_1.default.writeFileSync(path.join(pathFile, fileName).toString(), file);
                ludyConsole.end("Descarga de archivo " + fileName + " completada.");
                console.timeEnd('time-downloading-file');
                resolve('file created in folder successfully');
            }
            catch (error) {
                console.log(error);
                reject(error);
            }
        });
    };
    /**
     * Save file txt whit orders by date
     */
    FileManagement.prototype.saveTxtOrdersByDate = function (params, data) {
        return new Promise(function (resolve, reject) {
            try {
                var jumpLine_1 = "\r\n";
                var route = path.join(params.pathRoute, String(params.company), String(params.trabTipo), "/" + params.trabTipo + ".txt").toString();
                var stream_1 = fs_1.default.createWriteStream(route, { flags: 'a' });
                data.forEach(function (item, index) {
                    stream_1.write(params.dateOrder + "," + item.Orderid + "," + item.status + ", " + jumpLine_1);
                });
                resolve("file " + params.trabTipo + ".txt created in folder successfully");
            }
            catch (error) {
                console.log(error);
                reject(error);
            }
        });
    };
    /**
     * Save file json
     */
    FileManagement.prototype.saveJsonFile = function (params, data) {
        return new Promise(function (resolve, reject) {
            try {
                var now = moment_1.default().format("DDMMYYYY");
                var route = path.join(params.pathRoute, String(params.company), String(params.trabTipo), "/" + now + "-" + uuid_1.v4() + ".json").toString();
                var stream = fs_1.default.createWriteStream(route, { flags: 'a' });
                stream.write(JSON.stringify(data));
                resolve("file json created in folder successfully");
            }
            catch (error) {
                console.log(error);
                reject(error);
            }
        });
    };
    return FileManagement;
}());
exports.default = FileManagement;
