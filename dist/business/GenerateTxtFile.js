"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GenerateTxtFile = void 0;
var DataLudyImpo_dto_1 = require("../DTO/DataLudyImpo.dto");
var fs_1 = __importDefault(require("fs"));
var path = require('path');
var globalConfig = require('../../index.json');
var dataLudyImpoDto = new DataLudyImpo_dto_1.DataLudyImpoDTO();
var GenerateTxtFile = /** @class */ (function () {
    function GenerateTxtFile() {
    }
    GenerateTxtFile.prototype.homologationData = function (dataRequestFile) {
        var _this = this;
        var arrayData = [];
        return new Promise(function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                dataRequestFile.forEach(function (element) { return __awaiter(_this, void 0, void 0, function () {
                    var objectData, ciclo;
                    return __generator(this, function (_a) {
                        objectData = {};
                        if (element[4] === null) {
                            reject("El campo ciclo/Zona no puede ser nullo");
                        }
                        ciclo = element[4].substring(0, 3);
                        dataLudyImpoDto.estructuraArchivoPlano = element[3];
                        dataLudyImpoDto.cicloZonaInmueble = ciclo;
                        dataLudyImpoDto.loteDeLectura = this.concatenaString("", 2, "0");
                        dataLudyImpoDto.anio = element[5];
                        dataLudyImpoDto.mes = String(element[6]).length < 2 ? "0" + element[6] : String(element[6]);
                        dataLudyImpoDto.nombreDelCliente = 1234567;
                        dataLudyImpoDto.direccionDelPredio = this.concatenaString(element[8], 30, " ");
                        dataLudyImpoDto.rutaRecorrido = this.concatenaString(String(element[9]), 14, "0");
                        dataLudyImpoDto.emplazamiento = 11;
                        dataLudyImpoDto.medidor = this.concatenaString(element[10], 30, " ");
                        dataLudyImpoDto.observacionDeLectura = this.concatenaString(element[11], 36, " ");
                        dataLudyImpoDto.nombreDelCliente2 = this.concatenaString(element[12], 40, " ");
                        dataLudyImpoDto.lecturaPrimerPeriodo = this.concatenaString("", 8);
                        dataLudyImpoDto.consumoPromedioPrimerPeriodo = this.concatenaString("", 6);
                        dataLudyImpoDto.observacionPrimerPeriodo = this.concatenaString("", 2);
                        dataLudyImpoDto.origenLecturaPrimerPeriodo = this.concatenaString("", 1);
                        dataLudyImpoDto.lecturaSegundoPeriodo = this.concatenaString("", 8);
                        dataLudyImpoDto.consumoPromedioSegundoPeriodo = this.concatenaString("", 6);
                        dataLudyImpoDto.observacionSegundoPeriodo = this.concatenaString("", 2);
                        dataLudyImpoDto.origenLecturaSegundoPeriodo = this.concatenaString("", 1);
                        dataLudyImpoDto.lecturaTercerPeriodo = this.concatenaString("", 8);
                        dataLudyImpoDto.consumoPromedioTercerPeriodo = this.concatenaString("", 6);
                        dataLudyImpoDto.observacionTercerPeriodo = this.concatenaString("", 2);
                        dataLudyImpoDto.origenLecturaTercerPeriodo = this.concatenaString("", 1);
                        dataLudyImpoDto.lecturaCuartoPeriodo = this.concatenaString("", 8);
                        dataLudyImpoDto.consumoPromedioCuartoPeriodo = this.concatenaString("", 6);
                        dataLudyImpoDto.observacionCuartoPeriodo = this.concatenaString("", 2);
                        dataLudyImpoDto.origenLecturaCuartoPeriodo = this.concatenaString("", 1);
                        dataLudyImpoDto.lecturaPeriodoAnterior = element[13];
                        dataLudyImpoDto.lecturaEsperadaInferior = element[14];
                        dataLudyImpoDto.lecturaEsperadaSuperior = element[15];
                        dataLudyImpoDto.marcaMedidor = element[10].split("-")[0];
                        /* dataLudyImpoDto.empresa = globalConfig?.company
                        dataLudyImpoDto.tipoDeTrabajo = globalConfig?.jobtype[0] */
                        Object.assign(objectData, JSON.parse(JSON.stringify(dataLudyImpoDto)));
                        arrayData.push(objectData);
                        return [2 /*return*/];
                    });
                }); });
                console.log("Total de filas en archivo : " + arrayData.length);
                resolve(arrayData);
                return [2 /*return*/];
            });
        }); });
    };
    /**
     * Metodo que genera archivo txt con la estructura que espera la operacion LudyImpo
     * @param {object[]} data Array de objetos con las ordenes a cargar de lecturas
     */
    GenerateTxtFile.prototype.generateTxtFile = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
            var jumpLine, nameFile, route, stream;
            return __generator(this, function (_a) {
                jumpLine = "\r\n";
                nameFile = "output";
                route = path.join(__dirname, nameFile + ".txt");
                stream = fs_1.default.createWriteStream(route, { flags: 'w' });
                data.forEach(function (item, index) {
                    stream.write("" + item.estructuraArchivoPlano + item.cicloZonaInmueble + item.loteDeLectura + item.anio + item.mes + item.nombreDelCliente + item.direccionDelPredio + item.rutaRecorrido + item.emplazamiento + item.medidor + item.observacionDeLectura + item.lecturaPrimerPeriodo + item.consumoPromedioPrimerPeriodo + item.observacionPrimerPeriodo + item.origenLecturaPrimerPeriodo + item.lecturaSegundoPeriodo + item.consumoPromedioSegundoPeriodo + item.observacionSegundoPeriodo + item.origenLecturaSegundoPeriodo + item.lecturaTercerPeriodo + item.consumoPromedioTercerPeriodo + item.observacionTercerPeriodo + item.origenLecturaTercerPeriodo + item.lecturaCuartoPeriodo + item.consumoPromedioCuartoPeriodo + item.observacionCuartoPeriodo + item.origenLecturaCuartoPeriodo + item.lecturaPeriodoAnterior + item.lecturaEsperadaInferior + item.lecturaEsperadaSuperior + item.marcaMedidor + jumpLine);
                });
                resolve("file " + nameFile + ".txt created in folder successfully");
                return [2 /*return*/];
            });
        }); });
    };
    /**
     * Metodo que genera un string concatenado y si el parametro long es igual al length del string devuelve el mismo string
     * @param {string} dataString string base
     * @param {number} long Longitud que debe tener el string
     * @param {string} concatenate String que se desea concatenar para completar la longitud solicitada, por defecto es "1"
     *
     */
    GenerateTxtFile.prototype.concatenaString = function (dataString, long, concatenate) {
        if (concatenate === void 0) { concatenate = "1"; }
        if (dataString.length === long) {
            return dataString;
        }
        else {
            var reduce = long - dataString.length;
            for (var index = 0; index < reduce; index++) {
                dataString += concatenate !== null && concatenate !== void 0 ? concatenate : "1";
            }
            return dataString;
        }
    };
    return GenerateTxtFile;
}());
exports.GenerateTxtFile = GenerateTxtFile;
