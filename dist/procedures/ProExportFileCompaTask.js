"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProExportFileCompaTask = void 0;
var OperatorEnum_1 = require("../Enum/OperatorEnum");
var LudyConsole_1 = __importDefault(require("../utils/LudyConsole"));
var OracleException_1 = require("../exceptions/OracleException");
var oracledb_1 = __importDefault(require("oracledb"));
oracledb_1.default.outFormat = oracledb_1.default.OUT_FORMAT_OBJECT;
oracledb_1.default.fetchAsString = [oracledb_1.default.DB_TYPE_RAW, oracledb_1.default.DATE, oracledb_1.default.NCLOB];
/* OracleDB.prefetchRows = 200 */
var ludyConsole = new LudyConsole_1.default();
var dataFetch;
var ProExportFileCompaTask = /** @class */ (function () {
    function ProExportFileCompaTask() {
    }
    /**
     * consulta el procedimiento
     */
    ProExportFileCompaTask.prototype.run = function (procedureName, companyId, trabTip, state, initDate, finishDate, crudEnum) {
        var _this = this;
        return new Promise(function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                try {
                    if (!procedureName)
                        throw new Error("El nombre del procedimiento es requerido");
                    if (!companyId)
                        throw new Error("El id de la compañía es requerido");
                    if (!trabTip)
                        throw new Error("El tipo de trabajo es requerido");
                    if (!initDate)
                        throw new Error("La fecha inicial es requerida");
                    if (!finishDate)
                        throw new Error("La fecha final es requerida");
                    if (!crudEnum)
                        throw new Error("El tipo de función es requerido");
                    oracledb_1.default.getPool().getConnection()
                        .then(function (connection) { return __awaiter(_this, void 0, void 0, function () {
                        var _this = this;
                        return __generator(this, function (_a) {
                            connection.execute("BEGIN " + procedureName + "(\n                            :inucompanyid,\n                            :inutrabtip,\n                            :inustate,\n                            :initdate, \n                            :finishdate,\n                            :oclorders,\n                            :onuerrorcode,\n                            :osberrormessage\n                            \n                        ); \n                        END;", {
                                inucompanyid: {
                                    type: oracledb_1.default.NUMBER,
                                    val: companyId
                                },
                                inutrabtip: {
                                    type: oracledb_1.default.NUMBER,
                                    val: trabTip
                                },
                                inustate: {
                                    type: oracledb_1.default.NUMBER,
                                    val: state
                                },
                                initdate: {
                                    type: oracledb_1.default.STRING,
                                    val: initDate
                                },
                                finishdate: {
                                    type: oracledb_1.default.STRING,
                                    val: finishDate
                                },
                                oclorders: {
                                    type: oracledb_1.default.CLOB,
                                    dir: oracledb_1.default.BIND_OUT
                                },
                                onuerrorcode: {
                                    type: oracledb_1.default.NUMBER,
                                    dir: oracledb_1.default.BIND_OUT,
                                },
                                osberrormessage: {
                                    type: oracledb_1.default.STRING,
                                    dir: oracledb_1.default.BIND_OUT
                                }
                            }, function (err, result) { return __awaiter(_this, void 0, void 0, function () {
                                var output, ordersRaw, error_1, except_1;
                                var _this = this;
                                return __generator(this, function (_a) {
                                    switch (_a.label) {
                                        case 0:
                                            _a.trys.push([0, 5, , 6]);
                                            if (err)
                                                throw new OracleException_1.OracleException(err.message);
                                            output = result.outBinds;
                                            if (output.osbErrorMessage != null)
                                                throw new Error(output.osbErrorMessage);
                                            ordersRaw = output === null || output === void 0 ? void 0 : output.oclorders;
                                            _a.label = 1;
                                        case 1:
                                            _a.trys.push([1, 3, , 4]);
                                            return [4 /*yield*/, ordersRaw.getData()];
                                        case 2:
                                            dataFetch = _a.sent();
                                            return [3 /*break*/, 4];
                                        case 3:
                                            error_1 = _a.sent();
                                            reject(error_1);
                                            return [3 /*break*/, 4];
                                        case 4:
                                            switch (crudEnum) {
                                                case OperatorEnum_1.OperatorEnum.COMMIT:
                                                    connection.commit(function (error) {
                                                        if (error)
                                                            throw new Error(error);
                                                        connection.close(function (e) { return __awaiter(_this, void 0, void 0, function () {
                                                            return __generator(this, function (_a) {
                                                                if (e)
                                                                    ludyConsole.error("Ocurri\u00F3 un error al cerrar la conexi\u00F3n: " + JSON.stringify(e));
                                                                resolve(dataFetch);
                                                                return [2 /*return*/];
                                                            });
                                                        }); });
                                                    });
                                                    break;
                                                case OperatorEnum_1.OperatorEnum.ROLLBACK:
                                                    connection.rollback(function (error) {
                                                        if (error)
                                                            throw new Error(error);
                                                        connection.close(function (e) { return __awaiter(_this, void 0, void 0, function () {
                                                            return __generator(this, function (_a) {
                                                                if (e)
                                                                    ludyConsole.error("Ocurri\u00F3 un error al cerrar la conexi\u00F3n: " + JSON.stringify(e));
                                                                resolve(dataFetch);
                                                                return [2 /*return*/];
                                                            });
                                                        }); });
                                                    });
                                                    break;
                                                case OperatorEnum_1.OperatorEnum.READ:
                                                    connection.close(function (e) { return __awaiter(_this, void 0, void 0, function () {
                                                        return __generator(this, function (_a) {
                                                            if (e)
                                                                ludyConsole.error("Ocurri\u00F3 un error al cerrar la conexi\u00F3n: " + JSON.stringify(e));
                                                            resolve(dataFetch);
                                                            return [2 /*return*/];
                                                        });
                                                    }); });
                                                    break;
                                                default:
                                                    connection.close(function (e) { return __awaiter(_this, void 0, void 0, function () {
                                                        return __generator(this, function (_a) {
                                                            if (e)
                                                                ludyConsole.error("Ocurri\u00F3 un error al cerrar la conexi\u00F3n: " + JSON.stringify(e));
                                                            resolve(dataFetch);
                                                            return [2 /*return*/];
                                                        });
                                                    }); });
                                                    break;
                                            }
                                            return [3 /*break*/, 6];
                                        case 5:
                                            except_1 = _a.sent();
                                            connection.rollback(function (error) {
                                                if (error)
                                                    throw new Error(error);
                                                connection.close(function (e) { return __awaiter(_this, void 0, void 0, function () {
                                                    return __generator(this, function (_a) {
                                                        if (e)
                                                            ludyConsole.error("Ocurri\u00F3 un error al cerrar la conexi\u00F3n: " + JSON.stringify(e));
                                                        reject(except_1.message);
                                                        return [2 /*return*/];
                                                    });
                                                }); });
                                            });
                                            return [3 /*break*/, 6];
                                        case 6: return [2 /*return*/];
                                    }
                                });
                            }); });
                            return [2 /*return*/];
                        });
                    }); }, function (error) {
                        reject(error);
                    });
                }
                catch (error) {
                    ludyConsole.error(JSON.stringify(error));
                    reject(error);
                }
                return [2 /*return*/];
            });
        }); });
    };
    return ProExportFileCompaTask;
}());
exports.ProExportFileCompaTask = ProExportFileCompaTask;
